import mongoose, {Document, Schema } from 'mongoose';

export interface IProductSchema extends Document{
    image_url : string;
    product_type : string;
    aws_bucket_name : string;
    createdOn : string;
}

const ProductSchema = new Schema({
    image_url : {
        type: String, 
        trim: true, 
    },

    product_type : {
        type:String,
        default:"GENERAL"
    },

    aws_bucket_name : {
        type:String,
    },

    createdOn: {
        type: Number, 
        default: Date.now()
    },

});

const Product = mongoose.model<IProductSchema>('product', ProductSchema);
export default Product;