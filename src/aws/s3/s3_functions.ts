import AWS from 'aws-sdk';
import express from 'express';

AWS.config.update(
    {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    }
);
const s3 = new AWS.S3();

export async function uploadFileToAWSS3(file :Express.Multer.File) {
    const fileKey = file.originalname;

    var params = {
        Body: file.buffer,
        Bucket: process.env.AWS_BUCKET_NAME ?? 'visakh-aws-test',
        Key: fileKey
    }

    await s3.putObject(params).promise();
    return fileKey;
}

export async function getSignedUrl(req: express.Request, res: express.Response){
 
    var options = <AWS.S3.GetObjectRequest>{
        Bucket    : process.env.AWS_BUCKET_NAME ?? 'visakh-aws-test',
        Key    : req.query.fileKey ?? "default.png",
    };

    res.attachment((req.query.fileKey as string) ?? "default.png");
    var fileStream = s3.getObject(options).createReadStream();
    fileStream.pipe(res);
}