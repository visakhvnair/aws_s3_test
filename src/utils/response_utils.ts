import {Response} from 'express';

export function responseHandler(response: Response,
    message: string,
    data: any = {}){
        
        response.json({ 
            statusCode : response.statusCode,
            message : message,
            data : data
        });
};

export function errorHandler(response: Response, 
    message: string,
    data: any = {}){
        
        response.json({
            statusCode : response.statusCode,
            message : message,
            data : data
        });
};