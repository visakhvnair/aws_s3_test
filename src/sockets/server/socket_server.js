const express = require('express');
const socketIO = require('socket.io');
const http = require('http')
const port = 3000
var app = express();
let server = http.createServer(app);

const io = socketIO(server, {
	cors: {
		origin: '*',
	}
});

// make connection with user from server side
io.on('connection', (socket) => {
	console.log('New user connected');
	//emit message from server to user
	console.log(socket.rooms);

	// listen for message from user
	socket.on('createMessage', (newMessage) => {
		console.log('newMessage', newMessage);
		console.log(socket);
	});

	// when server disconnects from user
	socket.on('disconnect', () => {
		console.log('disconnected from user');
	});

	
});

server.listen(port);

export function alertFileUploaded(to) {
	io.to(to).emit('createMessage', "Your File uploaded" );
}
