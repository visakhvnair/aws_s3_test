import {Router} from 'express';
import * as productController from './products_controller';

import multer from 'multer';
var storage = multer.memoryStorage();
const upload = multer({ dest: 'uploads/' , storage: storage })

const router = Router();


router.post('/uploadProductImage',upload.single('image_file'), productController.uploadProductImage)

router.get('/fileStream', productController.getFileStream)


const product = module.exports = router;
export default product;