import express from 'express';
import { getSignedUrl, uploadFileToAWSS3 } from '../../aws/s3/s3_functions';
import { createNewProduct } from '../../db/product_db_function';
import { errorHandler, responseHandler } from '../../utils/response_utils';

export async function uploadProductImage(req: express.Request, res: express.Response) {
    try {
        if (req.file) {
            const fileKey = await uploadFileToAWSS3(req.file);

            var product = await createNewProduct(fileKey);
            // product = product.toObject();
            // if(product.aws_bucket_name) delete product.aws_bucket_name;
            
            res.status(200);
            return responseHandler(res, "File Uploaded", product);
        } else {
            res.status(406);
            return errorHandler(res, "No file added");
        }

    } catch (e) {
        res.status(501);
        return errorHandler(res, "Something went wrong. Please try again");
    }
}

export async function getFileStream(req: express.Request, res: express.Response) {
    getSignedUrl(req, res);
}