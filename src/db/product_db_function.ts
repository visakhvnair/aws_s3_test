import Product from "../schema/ProductSchema";

export async function createNewProduct(url:string) {
    const product = await Product.create(
        { 
            image_url: url, 
            aws_bucket_name : process.env.AWS_BUCKET_NAME ?? 'visakh-aws-test'
        });
    return product;
}